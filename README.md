## OpenHIM-load-ca

Basic utility to load a Certificate Authority cert into a running OpenHIM instance.

Most of the code is copied and modified from the [OpenHIM API documentation site][openhim-api].

[openhim-api]: http://openhim.org/docs/api/certificates/create

# Requirements

- docker
- docker-compose

# Quickstart

1. Build the image: `docker-compose build`
2. Update `config/config.json` with the needed credentials for your OpenHIM instance.
3. Place the CA to load, in PEM format, in the file `config/ca.crt`.
4. Run the loaded: `docker-compose run --rm load`