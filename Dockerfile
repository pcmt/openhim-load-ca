FROM node:18-bullseye

WORKDIR /app
RUN npm install request

ADD src/ /app

ENTRYPOINT ["node", "/app/load-ca.js"];
